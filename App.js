import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';


import SplashScreen from './app/components/Screens/SplashScreen';



const AppNavigator = createStackNavigator(
  {
    SplashScreen: { screen: SplashScreen },
    //ProfileScreen: { screen: ProfileScreen },
  },
  {
    initialRouteName: 'SplashScreen',
    headerMode: 'none',
  }

);

export default class App extends Component {




  render() {
    return (
      <AppNavigator />
    );
  }


}






const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
