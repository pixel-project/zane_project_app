import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import CardItem from '../Card/CardItem';

export default class SplashScreen extends Component<{}> {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            refresh: false,
        }
    }

    onPressOption = (profileOption) => {
        console.log("person clicked");
    };


    render() {
        return (
            <View style={styles.container}>
                <Text>Hello world!</Text>
                <ScrollView  >
                    <CardItem

                        firstName="Zane"
                        lastName="Dickens"
                        profilePic="zane.jpg"
                        onPressItem={this.onPressOption}

                    />
                    <CardItem

                        firstName="Lisa"
                        lastName="Dickens"
                        profilePic="lisa.jpg"
                        onPressItem={this.onPressOption}

                    />
                    <CardItem

                        firstName="Lisa"
                        lastName="Dickens"
                        profilePic="lisa.jpg"
                        onPressItem={this.onPressOption}

                    />
                    <CardItem

                        firstName="Lisa"
                        lastName="Dickens"
                        profilePic="lisa.jpg"
                        onPressItem={this.onPressOption}

                    />
                    <CardItem

                        firstName="Lisa"
                        lastName="Dickens"
                        profilePic="lisa.jpg"
                        onPressItem={this.onPressOption}

                    />
                    <CardItem

                        firstName="Lisa"
                        lastName="Dickens"
                        profilePic="lisa.jpg"
                        onPressItem={this.onPressOption}

                    />
                </ScrollView>
            </View>
        );





    }

}

const styles = StyleSheet.create(
    {

        cardStack: {
            flex: 0,
            justifyContent: 'center',
            flexDirection: 'column',
        },

        container: {
            flex: 1,
            justifyContent: 'center',
            flexDirection: 'row',
        },


    }
);