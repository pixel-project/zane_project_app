import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View
} from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class CardItem extends PureComponent {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            refresh: false,
        }
    }


    render() {

        const { index } = this.props
        const { firstName } = this.props
        const { lastName } = this.props
        const { profilePic } = this.props

        return (
            <View style={styles.container}>
                <View>
                    <Image
                    //style={styles.image}
                    //source={profilePic}
                    />
                    <View style={styles.fullName}>
                        <Text>{firstName}</Text>
                        <Text>{lastName}</Text>
                    </View>
                </View>
                <Text>{lastName}</Text>
            </View>
        );

    }

}

const styles = StyleSheet.create(
    {

        container: {

            justifyContent: 'center',
            flexDirection: 'column',
            height: 100,
            width: 200,
            marginTop: 40,
            padding: 10,
            backgroundColor: '#A5D1AE',
            borderRadius: 10,
            borderWidth: 2,
            borderColor: '#A5D1AE'
        },

        fullName: {
            flex: 1,
            justifyContent: 'center',
            flexDirection: 'column',


        }


    }
);